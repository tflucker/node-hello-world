const fs = require('fs');
const assert = require('assert');


describe('basic test', () => {
    var x = 0;
    
    it('x equals 10', async () => {
        x = 10;
        assert.equal(x, 10, "Some error message");
    });

    it('x not equals 10', async () => {
        x = 1;
        assert.notEqual(x, 10, "Second error message");
    });

});
