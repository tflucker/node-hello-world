module.exports = {
    preset: 'jest-preset-angular',
    setupTestFrameworkScriptFile: '<rootDir>/jest/setupJest.ts',
    testMatch: ['**/+(*.)+(spec|test).+(ts|js)?(x)'],
  
    transform: {
      '^.+\\.(ts|html)$': 'jest-preset-angular/preprocessor.js',
      '^.+\\.js$': 'babel-jest'
    },
    globals: {
      __TS_CONFIG__: {
        target: 'es6',
        module: 'commonjs',
        moduleResolution: 'node'
      },
      'ts-jest': {
        tsConfigFile: 'tsconfig.spec.json'
      },
      __TRANSFORM_HTML__: true,
      testResultsProcessor: './node_modules/jest-html-reporter'
    },
    transformIgnorePatterns: ['node_modules/(?!@ngrx)'],
    collectCoverageFrom: [
      '{apps|libs}/**/src/**/*.ts',
      '!**/*.d.ts',
      '!jest/*.ts',
      '!{apps|libs}/*-e2e/**',
      '!{apps|libs}/**/src/hmr.ts',
      '!{apps|libs}/**/src/**/constants/*.ts',
      '!{apps|libs}/**/src/**/index.ts',
      '!{apps|libs}/**/src/**/*module.ts',
      '!apps/**/src/environments/*.ts',
      '!{apps|libs}/**/src/main.ts',
      '!{apps|libs}/**/src/test.ts',
      '!apps/**/src/polyfills.ts'
    ],
    moduleFileExtensions: ['ts', 'js', 'html'],
  
    coverageThreshold: {
      global: {
        branches: 60,
        functions: 60,
        lines: 60,
        statements: -100
      }
    },
    resolver: '@nrwl/builders/plugins/jest/resolver'
  };