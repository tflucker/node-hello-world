# node-hello-world

Basic Node.js Express template project with page that says 'Hello World';


# Purpose 

This project is meant to be a sandbox while the Atlas Development team gains experience with Jenkins and setting up CI/CD pipelines for Node.js projects.