// // this is for .babelrc
// {
//     // See https://babeljs.io/docs/en/babel-preset-env#targets
//     "presets": [["@babel/preset-env", {"targets": {"node": "current"}}]]
// }


// this is for the babel.config.js
  module.exports = function (api) {
    api.cache(true);
  
    const presets = [['@babel/preset-env', {targets: {node: 'current'}}],"jest" ];
    const plugins = [["@babel/plugin-transform-async-to-generator"],
      ["@babel/plugin-transform-modules-commonjs", {
        "allowTopLevelThis": true
      }]
    ];
  
    return {
      presets,
      plugins
    };
  }