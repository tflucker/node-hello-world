#!/bin/sh

echo 'Beginning Docker build + deploy script'

# build image with tag (:latest)
docker build -t node-hello-world .
echo 'New image built ...'
echo 'Logging into Docker with ECR credentials'

# get login information from aws and run the output which is a docker login command
$(aws ecr get-login --no-include-email --region us-east-1)

echo 'Tagging newest build with ECR URI'

# tag the latest local build with your ECR URI
docker tag node-hello-world:latest 940093668739.dkr.ecr.us-east-1.amazonaws.com/node-hello-world:latest

echo 'Pushing newest image to ECR'

# push the image tagged in the previous command to your ECR, will become the "latest" version
docker push 940093668739.dkr.ecr.us-east-1.amazonaws.com/node-hello-world:latest

echo 'Completed Docker build + deploy script'